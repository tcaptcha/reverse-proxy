FROM ubuntu:22.04

WORKDIR /app

ADD local.yaml .
COPY . .

EXPOSE 5050/tcp

CMD ["./reverse-proxy"]
