package config

type AppCfg struct {
	Port     *int64   `yaml:"port"`
	Backends []string `yaml:"backends,flow"`
}
