.DEFAULT_GOAL := build

.PHONY: build
build:
	CGO_ENABLED=1 GO111MODULE=on GOPROXY=direct GOSUMDB=off GOARCH=amd64 go build

.PHONY: clear
clear:
	rm reverse-proxy

