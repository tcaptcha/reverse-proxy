package balancer

import (
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/tcaptcha/reverse-proxy/metadata"
	"gitlab.com/tcaptcha/reverse-proxy/pool"
)

type LoadBalancer struct {
	logger     *logrus.Logger
	serverPool *pool.ServerPool
}

func NewLoadBalancer(logger *logrus.Logger, serverPool *pool.ServerPool) *LoadBalancer {

	return &LoadBalancer{
		logger:     logger,
		serverPool: serverPool,
	}
}

// Balance load balances the incoming request
func (l *LoadBalancer) Balance(w http.ResponseWriter, r *http.Request) {
	attempts := metadata.GetAttemptsFromContext(r.Context())
	if attempts > 3 {
		l.logger.Errorf("%s(%s) max attempts reached, terminating\n", r.RemoteAddr, r.URL.Path)
		http.Error(w, "Service not available", http.StatusServiceUnavailable)

	} else if peer := l.serverPool.GetNextPeer(); peer == nil {
		http.Error(w, "Service not available", http.StatusServiceUnavailable)

	} else if stop := r.URL.Query().Get("stop"); stop == "stop" {
		http.Error(w, "Service is forbidden", http.StatusForbidden)

	} else {
		peer.ReverseProxy.ServeHTTP(w, r)

	}
}
