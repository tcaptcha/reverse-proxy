package logger

import (
	"os"
	"time"

	"github.com/sirupsen/logrus"
)

func initServerLogger() *logrus.Logger {
	var logger = logrus.New()

	// Output to stdout instead of the default stderr
	logger.Out = os.Stdout

	logger.Level = logrus.TraceLevel
	logger.SetReportCaller(true)

	logger.Formatter = &logrus.JSONFormatter{
		DisableTimestamp: false,
		TimestampFormat:  time.RFC3339Nano,
		FieldMap: logrus.FieldMap{
			logrus.FieldKeyFile:  "file",
			logrus.FieldKeyLevel: "level",
			logrus.FieldKeyMsg:   "msg",
			logrus.FieldKeyFunc:  "func",
		},
	}

	return logger
}

func initDefaultLogger() *logrus.Logger {
	var logger = logrus.New()

	// Output to stdout instead of the default stderr
	logger.Out = os.Stdout

	logger.Level = logrus.TraceLevel

	return logger
}

func InitLogger(isServer bool) *logrus.Logger {
	if isServer {

		return initServerLogger()
	} else {

		return initDefaultLogger()
	}
}
