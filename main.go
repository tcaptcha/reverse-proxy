package main

import (
	"fmt"
	"net/http"
	"net/http/httputil"
	"net/url"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/tcaptcha/reverse-proxy/balancer"
	"gitlab.com/tcaptcha/reverse-proxy/config"
	log "gitlab.com/tcaptcha/reverse-proxy/logger"
	"gitlab.com/tcaptcha/reverse-proxy/metadata"
	"gitlab.com/tcaptcha/reverse-proxy/pool"
)

// healthCheck runs a routine for check status of the backends every 2 mins
func healthCheck(logger *logrus.Logger, serverPool *pool.ServerPool) {
	t := time.NewTicker(time.Minute * 2)
	for {
		select {
		case <-t.C:
			logger.Debugf("Starting health check...")
			serverPool.HealthCheck()

		}
	}
}

func main() {
	logger := log.InitLogger(true) // TODO: true
	appCfg := config.LoadConfig(logger, "local.yaml")

	if len(appCfg.Backends) == 0 {

		logger.Fatal("Please provide one or more backends to load balance")

	}

	serverPool := pool.NewServerPool(logger)
	lb := balancer.NewLoadBalancer(logger, serverPool)

	// parse servers
	for _, backendsUrl := range appCfg.Backends {
		backendUrl, err := url.Parse(backendsUrl)
		if err != nil {

			logger.Fatal(err)

		}

		proxy := httputil.NewSingleHostReverseProxy(backendUrl)
		proxy.ErrorHandler = func(writer http.ResponseWriter, request *http.Request, e error) {
			logger.Warnf("[%s] %s\n", backendUrl.Host, e.Error())

			retries := metadata.GetRetryFromContext(request.Context())
			if retries < 3 {
				select {
				case <-time.After(10 * time.Millisecond):
					ctx := metadata.SetRetryToContext(request.Context(), retries+1)
					proxy.ServeHTTP(writer, request.WithContext(ctx))
				}

				return
			}

			// after 3 retries, mark this backend as down
			serverPool.MarkBackendStatus(backendUrl, false)

			// if the same request routing for few attempts with different backends, increase the count
			attempts := metadata.GetAttemptsFromContext(request.Context())
			logger.Warnf("%s(%s) attempting retry %d\n", request.RemoteAddr, request.URL.Path, attempts)

			ctx := metadata.SetAttemptsToContext(request.Context(), attempts+1)

			lb.Balance(writer, request.WithContext(ctx))
		}

		serverPool.AddBackend(pool.NewBackendServer(backendUrl, proxy, true))

		logger.Infof("configured server: %s\n", backendUrl)
	}

	// start health checking
	go healthCheck(logger, serverPool)

	// create http server
	server := http.Server{
		Addr:    fmt.Sprintf(":%d", *appCfg.Port),
		Handler: http.HandlerFunc(lb.Balance),
	}

	logger.Infof("Load Balancer started at :%d\n", *appCfg.Port)
	if err := server.ListenAndServe(); err != nil {

		logger.Fatal(err)

	}
}
