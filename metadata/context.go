package metadata

import (
	"context"
)

const (
	attempts string = "attempts"
	retry    string = "retry"
)

// GetAttemptsFromContext returns the attempts for request
func GetAttemptsFromContext(ctx context.Context) int {
	if attempts, ok := ctx.Value(attempts).(int); ok {

		return attempts
	} else {

		return 1
	}
}

// SetAttemptsToContext set attempts to the context
func SetAttemptsToContext(ctx context.Context, newAttempts int) context.Context {

	return context.WithValue(ctx, attempts, newAttempts)
}

// GetRetryFromContext returns the retries for request
func GetRetryFromContext(ctx context.Context) int {
	if retry, ok := ctx.Value(retry).(int); ok {

		return retry
	} else {

		return 0
	}
}

// SetRetryToContext set retries to the context
func SetRetryToContext(ctx context.Context, newRetries int) context.Context {
	return context.WithValue(ctx, retry, newRetries)

}
