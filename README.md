Install Go
-----------
https://www.digitalocean.com/community/tutorials/how-to-install-go-on-ubuntu-20-04


Prepare app
---------------

- `mkdir tcaptcha`
- `cd tcaptcha`
- `git clone git@gitlab.com:tcaptcha/reverse-proxy.git`

edit local.yaml

Build app
----------

- `CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go mod tidy`
- `CGO_ENABLED=1 GOOS=linux GOARCH=amd64 go build`
- `docker build -t reverse-proxy .`
- `docker run -d --rm --network host reverse-proxy`

Run app
--------


Build with CI
----------
- `CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go mod tidy`
- `CGO_ENABLED=1 GOOS=linux GOARCH=amd64 go build`
- `docker build -t $TAG_COMMIT -t $TAG_LATEST .`
- `docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY`
- `docker push $TAG_COMMIT`
- `docker push $TAG_LATEST`

Run with CI
------------
- `docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY`
- `docker pull $TAG_COMMIT`
- `docker container rm -f reverse-proxy || true`
- `docker run -d --rm --log-driver=journald --network host --name reverse-proxy $TAG_COMMIT`


