package pool

import (
	"net/http/httputil"
	"net/url"
	"sync"
)

// BackendServer holds the data about a server
type BackendServer struct {
	URL          *url.URL
	isAlive      bool
	ReverseProxy *httputil.ReverseProxy
	mu           *sync.RWMutex
}

func NewBackendServer(url *url.URL, proxy *httputil.ReverseProxy, isAlive bool) *BackendServer {

	return &BackendServer{
		URL:          url,
		isAlive:      isAlive,
		ReverseProxy: proxy,
		mu:           new(sync.RWMutex),
	}
}

// SetAlive for this backend
func (b *BackendServer) SetAlive(alive bool) {
	b.mu.Lock()
	defer b.mu.Unlock()

	b.isAlive = alive
}

// IsAlive returns true when backend is alive
func (b *BackendServer) IsAlive() bool {
	b.mu.RLock()
	defer b.mu.RUnlock()

	return b.isAlive
}
