package pool

import (
	"log"
	"net"
	"net/url"
	"sync/atomic"
	"time"

	"github.com/sirupsen/logrus"
)

// ServerPool holds information about reachable backends
type ServerPool struct {
	backends []*BackendServer
	current  uint64
	logger   *logrus.Logger
}

func NewServerPool(logger *logrus.Logger) *ServerPool {

	return &ServerPool{
		backends: nil,
		current:  0,
		logger:   logger,
	}
}

// AddBackend to the server pool
func (s *ServerPool) AddBackend(backend *BackendServer) {
	s.backends = append(s.backends, backend)

}

// NextIndex atomically increase the counter and return an index
func (s *ServerPool) NextIndex() int {

	return int(atomic.AddUint64(&s.current, uint64(1)) % uint64(len(s.backends)))
}

// MarkBackendStatus changes a status of a backend
func (s *ServerPool) MarkBackendStatus(backendUrl *url.URL, alive bool) {
	for _, b := range s.backends {
		if b.URL.String() == backendUrl.String() {
			b.SetAlive(alive)
			break
		}
	}
}

// GetNextPeer returns next active peer to take a connection
func (s *ServerPool) GetNextPeer() *BackendServer {
	// loop entire backends to find out an Alive backend
	next := s.NextIndex()
	l := len(s.backends) + next // start from next and move a full cycle
	for i := next; i < l; i++ {
		idx := i % len(s.backends)     // take an index by modding
		if s.backends[idx].IsAlive() { // if we have an alive backend, use it and store if its not the original one
			if i != next {
				atomic.StoreUint64(&s.current, uint64(idx))
			}
			return s.backends[idx]
		}
	}
	return nil
}

// HealthCheck pings the backends and update the status
func (s *ServerPool) HealthCheck() {
	for _, b := range s.backends {
		status := "up"
		alive := isBackendAlive(b.URL)
		b.SetAlive(alive)
		if !alive {
			status = "down"
		}
		log.Printf("%s [%s]\n", b.URL, status)
	}
}

// isBackendAlive checks whether a backend is Alive by establishing a TCP connection
func isBackendAlive(u *url.URL) bool {
	timeout := 2 * time.Second
	if conn, err := net.DialTimeout("tcp", u.Host, timeout); err != nil {
		log.Println("Site unreachable, error: ", err)

		return false
	} else {
		if err := conn.Close(); err != nil {

			log.Println("failed connection close, error: ", err)
		}

		return true

	}
}
